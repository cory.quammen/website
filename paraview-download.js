// ----------------------------------------------------------------------------
//                           Drop down dependency
// ----------------------------------------------------------------------------

var whichVersion = new DynamicOptionList('version', 'type', 'os', 'downloadFile');
whichVersion.addDependentFields('version','type');
whichVersion.addDependentFields('type', 'os');
whichVersion.addDependentFields('os', 'downloadFile');
whichVersion.selectFirstOption = true;

var versions = "<option value='v5.2' selected='true'>v5.2</option>"
             + "<option value='v5.1'>v5.1</option>"
             + "<option value='v5.0'>v5.0</option>"
	           + "<option value='v4.4'>v4.4</option>"
             + "<option value='v4.3'>v4.3</option>"
             + "<option value='v4.2'>v4.2</option>"
             + "<option value='v4.1'>v4.1</option>"
             + "<option value='v4.0'>v4.0</option>"
             + "<option value='v3.98'>v3.98</option>"
             + "<option value='v3.14'>v3.14</option>"
             + "<option value='v3.12'>v3.12</option>"
             + "<option value='v3.10'>v3.10</option>"
             + "<option value='v3.9'>v3.9</option>"
             + "<option value='v3.8'>v3.8</option>"
             + "<option value='v3.6'>v3.6</option>"
             + "<option value='v3.4'>v3.4</option>"
             + "<option value='v3.2'>v3.2</option>"
             + "<option value='v3.1'>v3.1</option>"
             + "<option value='v3.0'>v3.0</option>"
             + "<option value='v2.9'>v2.9</option>"
             + "<option value='v2.6'>v2.6</option>"
             + "<option value='v2.5'>v2.5</option>"
             + "<option value='v2.4'>v2.4</option>"
             + "<option value='v2.2'>v2.2</option>"
             + "<option value='v2.0'>v2.0</option>"
             + "<option value='v1.8'>v1.8</option>"
             + "<option value='v1.6'>v1.6</option>"
             + "<option value='v1.4'>v1.4</option>"
             + "<option value='v1.2'>v1.2</option>"
             + "<option value='v1.0'>v1.0</option>";

// ----------------------------------------------------------------------------
//                                Labels
// ----------------------------------------------------------------------------

var labels = {
  "binary"  : "ParaView Binary Installers",
  "source"  : "ParaView Source Files",
  "data"    : "Data, Documentation, and Tutorials",
  "plugin"  : 'Community Contributed Plugins',
  "app"     : 'Community Contributed Applications',
  "catalyst": 'Catalyst Editions',
  "win64"   : "Windows 64-bit",
  "win32"   : "Windows 32-bit",
  "linux64" : "Linux 64-bit",
  "linux32" : "Linux 32-bit",
  "osx"     : "Mac OS X",
  "all"     : "All",
  "nightly" : "Nightly",
  "demos"   : "Experimental Demos"
};

// ----------------------------------------------------------------------------
// Fill dynamic drop down
// ----------------------------------------------------------------------------

function updateDynamicOptionList(version, data) {
  fileList = [];
  for(var type in data) {
    whichVersion.forValue(version).addOptionsTextValue(labels[type], type);
    for(var os in data[type]) {
      whichVersion.forValue(version).forValue(type).addOptionsTextValue(labels[os], os);
      for(var fileIdx in data[type][os]) {
        fileName = data[type][os][fileIdx];
        fileList.push(fileName);
        whichVersion.forValue(version).forValue(type).forValue(os).addOptions(fileName);
      }
    }
  }
  return fileList;
}

// ----------------------------------------------------------------------------
// Direct Link code base
// ----------------------------------------------------------------------------

var downloadLinkUpdateRequest = 0;

// ----------------------------------------------------------------------------

function updateDownloadLinks() {
  downloadLinkUpdateRequest = 0;

  // Update generic download link
  var url = 'http://www.paraview.org/paraview-downloads/download.php?submit=Download';
  jQuery('.generic select').each(function(){
    var me = jQuery(this);
    url += '&' + me.attr('name') + '=' + me.val();
  });
  htmlContent = "<a href='" + url + "'>" + jQuery("select[name=downloadFile]").val() + "</a>";
  jQuery('.generic .direct-download-link')[0].innerHTML = htmlContent;

  // Update nightly download link
  url = 'http://www.paraview.org/paraview-downloads/download.php?submit=Download&version=nightly&type=all&os=all&downloadFile=';
  fileName = jQuery(".nightly select[name=downloadFile]").val();
  htmlContent = "<a href='" + url + fileName + "'>" + fileName + "</a>";
  jQuery('.nightly .direct-download-link')[0].innerHTML = htmlContent;
}

// ----------------------------------------------------------------------------

function invalidateDownloadLink() {
  jQuery('.direct-download-link').hide();
  if(downloadLinkUpdateRequest === 0) {
    downloadLinkUpdateRequest = setTimeout(updateDownloadLinks, 100);
  }
}

// ----------------------------------------------------------------------------
// Pre-select current browser OS
// ----------------------------------------------------------------------------

function updateOSToCurrent() {
  var os = "";
  if (navigator.appVersion.indexOf("Win") != -1) {
    os = "win";
    if(navigator.appVersion.indexOf("64") != -1) {
      os += "64";
    } else {
      os += "32"
    }
  } else if (navigator.appVersion.indexOf("Mac")!=-1) {
    os = "osx";
  } else if (navigator.appVersion.indexOf("X11") != -1 || navigator.appVersion.indexOf("Linux") != -1) {
    os = "linux";
    if(navigator.appVersion.indexOf("64") != -1) {
      os += "64";
    } else {
      os += "32"
    }
  }  // Update OS if possible
  if(jQuery('select[name=os] > option[value=' + os + ']').length > 0) {
    jQuery('select[name=os]').val(os).trigger('change');
  }
}

// ----------------------------------------------------------------------------

function updateToCurrentOSLater() {
  setTimeout(updateOSToCurrent, 200);
}


// ----------------------------------------------------------------------------
// ParaView 5.2.0
// ----------------------------------------------------------------------------
updateDynamicOptionList( "v5.2", {
  "binary" : {
             "win64"    : [ "ParaView-5.2.0-RC4-Qt4-OpenGL2-Windows-64bit.exe",
                            "ParaView-5.2.0-RC4-Qt4-OpenGL2-Windows-64bit.zip",
                            "ParaView-5.2.0-RC4-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                            "ParaView-5.2.0-RC4-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-Windows-64bit.exe",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-Windows-64bit.zip",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-Windows-64bit.exe",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-Windows-64bit.zip",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-Windows-64bit.exe",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-Windows-64bit.zip",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-MPI-Windows-64bit.zip"
                          ],
             "win32"    : [
                            "ParaView-5.2.0-RC4-Qt4-OpenGL2-Windows-32bit.exe",
                            "ParaView-5.2.0-RC4-Qt4-OpenGL2-Windows-32bit.zip",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-Windows-32bit.exe",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-Windows-32bit.zip",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-Windows-32bit.exe",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-Windows-32bit.zip",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-Windows-32bit.exe",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-Windows-32bit.zip"
                          ],
             "linux64"  : [
                            "ParaView-5.2.0-RC4-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz"
                          ],
             "osx"      : [
                            "ParaView-5.2.0-RC4-Qt4-OpenGL2-MPI-OSX10.8-64bit.dmg",
                            "ParaView-5.2.0-RC3-Qt4-OpenGL2-MPI-OSX10.8-64bit.dmg",
                            "ParaView-5.2.0-RC2-Qt4-OpenGL2-MPI-OSX10.8-64bit.dmg",
                            "ParaView-5.2.0-RC1-Qt4-OpenGL2-MPI-OSX10.8-64bit.dmg"
                          ]
             },
  "source" : {
            "all"     : [
                          "ParaView-v5.2.0-RC4.tar.gz",
                          "ParaView-v5.2.0-RC4.zip",
                          "ParaView-v5.2.0-RC3.tar.gz",
                          "ParaView-v5.2.0-RC3.zip",
                          "ParaView-v5.2.0-RC2.tar.gz",
                          "ParaView-v5.2.0-RC2.zip",
                          "ParaView-v5.2.0-RC1.tar.gz",
                          "ParaView-v5.2.0-RC1.zip"
                        ]},
  "data"   : {
             "all"   : [
                          "ParaViewGuide-5.2.0-RC4.pdf",
                          "ParaViewGuide-5.2.0-RC3.pdf",
                          "ParaViewGuide-5.2.0-RC2.pdf",
                          "ParaViewGuide-5.2.0-RC1.pdf",
                          "ParaViewCatalystGuide-5.2.0-RC4.pdf",
                          "ParaViewCatalystGuide-5.2.0-RC3.pdf",
                          "ParaViewCatalystGuide-5.2.0-RC2.pdf",
                          "ParaViewCatalystGuide-5.2.0-RC1.pdf",
                          "ParaViewGettingStarted-v5.2.0-RC4.pdf",
                          "ParaViewGettingStarted-v5.2.0-RC3.pdf",
                          "ParaViewGettingStarted-v5.2.0-RC2.pdf",
                          "ParaViewGettingStarted-v5.2.0-RC1.pdf",
                          "ParaViewTutorial.pdf",
                          "ParaViewData-v5.2.0-RC4.tar.gz",
                          "ParaViewData-v5.2.0-RC3.tar.gz",
                          "ParaViewData-v5.2.0-RC2.tar.gz",
                          "ParaViewData-v5.2.0-RC1.tar.gz",
                          "ParaViewData-v5.2.0-RC4.zip",
                          "ParaViewData-v5.2.0-RC3.zip",
                          "ParaViewData-v5.2.0-RC2.zip",
                          "ParaViewData-v5.2.0-RC1.zip"
                       ]},
  "plugin" : {
             "win64"    : [
                            "AcuSolveReaderPlugin-5.2.0-RC4-Qt4-OpenGL2-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC4-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC3-Qt4-OpenGL2-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC3-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC2-Qt4-OpenGL2-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC2-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC1-Qt4-OpenGL2-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC1-Qt4-OpenGL2-MPI-Windows-64bit.zip"
                          ],
             "win32"    : [
                            "AcuSolveReaderPlugin-5.2.0-RC4-Qt4-OpenGL2-Windows-32bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC3-Qt4-OpenGL2-Windows-32bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC2-Qt4-OpenGL2-Windows-32bit.zip",
                            "AcuSolveReaderPlugin-5.2.0-RC1-Qt4-OpenGL2-Windows-32bit.zip"
                          ],
             "linux64"  : [
                            "AcuSolveReaderPlugin-5.2.0-RC4-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                            "AcuSolveReaderPlugin-5.2.0-RC3-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                            "AcuSolveReaderPlugin-5.2.0-RC2-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                            "AcuSolveReaderPlugin-5.2.0-RC1-Qt4-OpenGL2-MPI-Linux-64bit.tgz"
                          ]
/*             "osx"      : [
                            "VisTrailsPlugin-5.2.0-RC1-Qt4-OpenGL2-MPI-OSX10.7-64bit.tgz"
                          ]*/
             },

  "catalyst" : {
              "all"  : [
                        "Catalyst-v5.2.0-RC4-HostTools.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base-Enable-Python.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base-Enable-Python-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base-Enable-Python-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC4-Base-Enable-Python-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.2.0-RC3-HostTools.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base-Enable-Python.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base-Enable-Python-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base-Enable-Python-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC3-Base-Enable-Python-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.2.0-RC2-HostTools.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base-Enable-Python.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base-Enable-Python-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base-Enable-Python-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC2-Base-Enable-Python-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.2.0-RC1-HostTools.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base-Enable-Python.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base-Enable-Python-Essentials.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base-Enable-Python-Essentials-Extras.tar.gz",
                        "Catalyst-v5.2.0-RC1-Base-Enable-Python-Essentials-Extras-Rendering-Base.tar.gz"
                       ]}
  });


// ----------------------------------------------------------------------------
// ParaView 5.1.0
// ----------------------------------------------------------------------------
updateDynamicOptionList( "v5.1", {
  "binary" : {
             "win64"    : [
                            "ParaView-5.1.2-Qt4-OpenGL2-Windows-64bit.exe",
                            "ParaView-5.1.2-Qt4-OpenGL2-Windows-64bit.zip",
                            "ParaView-5.1.2-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                            "ParaView-5.1.2-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "ParaView-5.1.0-Qt4-OpenGL2-Windows-64bit.exe",
                            "ParaView-5.1.0-Qt4-OpenGL2-Windows-64bit.zip",
                            "ParaView-5.1.0-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                            "ParaView-5.1.0-Qt4-OpenGL2-MPI-Windows-64bit.zip"
                          ],
             "win32"    : [
                            "ParaView-5.1.2-Qt4-OpenGL2-Windows-32bit.exe",
                            "ParaView-5.1.2-Qt4-OpenGL2-Windows-32bit.zip",
                            "ParaView-5.1.0-Qt4-OpenGL2-Windows-32bit.exe",
                            "ParaView-5.1.0-Qt4-OpenGL2-Windows-32bit.zip"
                          ],
             "linux64"  : [
                            "ParaView-5.1.2-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz",
                            "ParaView-5.1.0-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz"
                          ],
             "osx"      : [
                            "ParaView-5.1.2-Qt4-OpenGL2-MPI-OSX10.7-64bit.dmg",
                            "ParaView-5.1.0-Qt4-OpenGL2-MPI-OSX10.7-64bit.dmg"
                          ]
             },
  "source" : {
            "all"     : [
                          "ParaView-v5.1.2.tar.gz",
                          "ParaView-v5.1.2.zip",
                          "ParaView-v5.1.0.tar.gz",
                          "ParaView-v5.1.0.zip"
                        ]},
  "data"   : {
             "all"   : [
                          "ParaViewData-v5.1.2.tar.gz",
                          "ParaViewData-v5.1.2.zip",
                          "ParaViewGuide-5.1.0.pdf",
                          "ParaViewGettingStarted-5.1.0.pdf",
                          "ParaViewTutorial.pdf",
                          "ParaViewData-v5.1.0.tar.gz",
                          "ParaViewData-v5.1.0.zip"
                       ]},
  "plugin" : {
             "win64"    : [
                            "AcuSolveReaderPlugin-5.1.2-Qt4-OpenGL2-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.1.2-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.1.0-Qt4-OpenGL2-Windows-64bit.zip",
                            "AcuSolveReaderPlugin-5.1.0-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "VisTrailsPlugin-5.1.2-Qt4-OpenGL2-Windows-64bit.zip",
                            "VisTrailsPlugin-5.1.2-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                            "VisTrailsPlugin-5.1.0-Qt4-OpenGL2-Windows-64bit.zip",
                            "VisTrailsPlugin-5.1.0-Qt4-OpenGL2-MPI-Windows-64bit.zip"
                          ],
             "win32"    : [
                            "AcuSolveReaderPlugin-5.1.2-Qt4-OpenGL2-Windows-32bit.zip",
                            "VisTrailsPlugin-5.1.2-Qt4-OpenGL2-Windows-32bit.zip",
                            "AcuSolveReaderPlugin-5.1.0-Qt4-OpenGL2-Windows-32bit.zip",
                            "VisTrailsPlugin-5.1.0-Qt4-OpenGL2-Windows-32bit.zip"
                          ],
             "linux64"  : [
                            "AcuSolveReaderPlugin-5.1.2-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                            "VisTrailsPlugin-5.1.2-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                            "AcuSolveReaderPlugin-5.1.0-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                            "VisTrailsPlugin-5.1.0-Qt4-OpenGL2-MPI-Linux-64bit.tgz"
                          ],
             "osx"      : [
                            "VisTrailsPlugin-5.1.2-Qt4-OpenGL2-MPI-OSX10.7-64bit.tgz",
                            "VisTrailsPlugin-5.1.0-Qt4-OpenGL2-MPI-OSX10.7-64bit.tgz"
                          ]
             },

  "catalyst" : {
              "all"  : [
                        "Catalyst-v5.1.2-HostTools.tar.gz",
                        "Catalyst-v5.1.2-Base.tar.gz",
                        "Catalyst-v5.1.2-Base-Essentials.tar.gz",
                        "Catalyst-v5.1.2-Base-Essentials-Extras.tar.gz",
                        "Catalyst-v5.1.2-Base-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.1.2-Base-Enable-Python.tar.gz",
                        "Catalyst-v5.1.2-Base-Enable-Python-Essentials.tar.gz",
                        "Catalyst-v5.1.2-Base-Enable-Python-Essentials-Extras.tar.gz",
                        "Catalyst-v5.1.2-Base-Enable-Python-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.1.0-HostTools.tar.gz",
                        "Catalyst-v5.1.0-Base.tar.gz",
                        "Catalyst-v5.1.0-Base-Essentials.tar.gz",
                        "Catalyst-v5.1.0-Base-Essentials-Extras.tar.gz",
                        "Catalyst-v5.1.0-Base-Essentials-Extras-Rendering-Base.tar.gz",
                        "Catalyst-v5.1.0-Base-Enable-Python.tar.gz",
                        "Catalyst-v5.1.0-Base-Enable-Python-Essentials.tar.gz",
                        "Catalyst-v5.1.0-Base-Enable-Python-Essentials-Extras.tar.gz",
                        "Catalyst-v5.1.0-Base-Enable-Python-Essentials-Extras-Rendering-Base.tar.gz"
                       ]},
  "demos"   : {
              "win64" : [ "ParaView-5.1.2-Occulus-Vive-Windows-64bit.zip" ]
              }
});


// ----------------------------------------------------------------------------
// ParaView 5.0.0
// ----------------------------------------------------------------------------
updateDynamicOptionList( "v5.0", {
  "binary" : {
            "win64"   : [
                          "ParaView-5.0.1-Qt4-OpenGL2-Windows-64bit.exe",
                          "ParaView-5.0.1-Qt4-OpenGL2-Windows-64bit.zip",
                          "ParaView-5.0.1-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                          "ParaView-5.0.1-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                          "ParaView-5.0.0-Qt4-OpenGL2-Windows-64bit.exe",
                          "ParaView-5.0.0-Qt4-OpenGL2-Windows-64bit.zip",
                          "ParaView-5.0.0-Qt4-OpenGL2-MPI-Windows-64bit.exe",
                          "ParaView-5.0.0-Qt4-OpenGL2-MPI-Windows-64bit.zip"
                        ],
            "win32"   : [
                          "ParaView-5.0.1-Qt4-OpenGL2-Windows-32bit.exe",
                          "ParaView-5.0.1-Qt4-OpenGL2-Windows-32bit.zip"
                        ],
            "linux64" : [
                          "ParaView-5.0.1-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz",
                          "ParaView-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz"
                        ],
            "osx"     : [
                          "ParaView-5.0.1-Qt4-OpenGL2-MPI-OSX10.7-64bit.dmg",
                          "ParaView-5.0.0-Qt4-OpenGL2-MPI-OSX10.7-64bit.dmg"
                        ]
             },
  "source" : {
            "all"     : [
                          "ParaView-v5.0.1-source.tar.gz",
                          "ParaView-v5.0.1-source.zip",
                          "ParaView-v5.0.0-source.tar.gz",
                          "ParaView-v5.0.0-source.zip"
                        ]},
  "data"   : {
             "all"   : [
                          "ParaViewGuide-CE-v5.0.0.pdf",
                          "ParaViewCatalystGuide-CE-v5.0.0.pdf",
                          "ParaViewData-5.0.0.tar.gz",
                          "ParaViewData-5.0.0.zip",
                          "ParaView-v5.0.0-API-doc.tar.gz"
                       ]},
  "plugin" : {
            "win64"  : [
                          "AcuSolveReaderPlugin-5.0.1-Qt4-OpenGL2-Windows-64bit.zip",
                          "AcuSolveReaderPlugin-5.0.1-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                          "AcuSolveReaderPlugin-5.0.0-Qt4-OpenGL2-Windows-64bit.zip",
                          "AcuSolveReaderPlugin-5.0.0-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                          "VisTrailsPlugin-5.0.1-Qt4-OpenGL2-Windows-64bit.zip",
                          "VisTrailsPlugin-5.0.1-Qt4-OpenGL2-MPI-Windows-64bit.zip",
                          "VisTrailsPlugin-5.0.0-Qt4-OpenGL2-Windows-64bit.zip",
                          "VisTrailsPlugin-5.0.0-Qt4-OpenGL2-MPI-Windows-64bit.zip"
                       ],
            "win32"  : [
                          "AcuSolveReaderPlugin-5.0.1-Qt4-OpenGL2-Windows-32bit.zip",
                          "VisTrailsPlugin-5.0.1-Qt4-OpenGL2-Windows-32bit.zip"
                       ],
            "linux64": [
                          "AcuSolveReaderPlugin-5.0.1-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                          "AcuSolveReaderPlugin-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                          "VisTrailsPlugin-5.0.1-Qt4-OpenGL2-MPI-Linux-64bit.tgz",
                          "VisTrailsPlugin-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit.tgz"
                       ],
            "osx"    : [
                          "VisTrailsPlugin-5.0.1-Qt4-OpenGL2-MPI-OSX10.7-64bit.tgz",
                          "VisTrailsPlugin-5.0.0-Qt4-OpenGL2-MPI-OSX10.7-64bit.tgz"
                       ]
             },
//  "catalyst" : {
//              "all"  : [
//                "Catalyst-base-v4.4.0.tar.gz",
//                "Catalyst-base+python-v4.4.0.tar.gz",
//                "Catalyst-base+essentials-v4.4.0.tar.gz",
//                "Catalyst-base+essentials+python-v4.4.0.tar.gz",
//                "Catalyst-base+essentials+extras-v4.4.0.tar.gz",
//                "Catalyst-base+essentials+extras+python-v4.4.0.tar.gz",
//                "Catalyst-base+essentials+extras+renderingbase-v4.4.0.tar.gz",
//                "Catalyst-base+essentials+extras+renderingbase+python-v4.4.0.tar.gz" ]
//               },
//  "app"    : {
//            "win64"  : [ "VeloView-3.1.1-26022015-Windows-64bit.exe"],
//            "win32"  : [ "VeloView-3.1.1-26022015-Windows-32bit.exe"],
//            "osx"    : [ "VeloView-3.1.1-26022015-Darwin-64bit.dmg"]
//             }
});




// ----------------------------------------------------------------------------
// ParaView 4.4.0
// ----------------------------------------------------------------------------

updateDynamicOptionList( "v4.4", {
  "binary" : {
            "win64"   : [ "ParaView-4.4.0-Qt4-Windows-64bit.exe",
			  "ParaView-4.4.0-Qt4-Windows-64bit.zip"],

            "win32"   : [ "ParaView-4.4.0-Qt4-Windows-32bit.exe",
                          "ParaView-4.4.0-Qt4-Windows-32bit.zip"],

            "linux64" : [ "ParaView-4.4.0-Qt4-Linux-64bit.tar.gz" ],
            "osx"     : [ "ParaView-4.4.0-Qt4-OSX10.7-64bit.dmg"]
             },
  "source" : {
            "all"     : [
                          "ParaView-v4.4.0-source.tar.gz",
                          "ParaView-v4.4.0-source.zip"
			]},
  "data"   : {
             "all"   : [
			"ParaViewData-v4.4.0.tar.gz",
			"ParaViewData-v4.4.0.zip",
                        //"TheParaViewGuide-v4.3-CC-Edition.pdf",
                        //"ParaView-v4.3.1-API-doc.tar.gz",
			//"ParaViewCatalystUsersGuide_v2.pdf"
                       ]},
  "plugin" : {
            "win64"  : ["AcuSolveReaderPlugin-4.4.0-Qt4-Windows-64bit.zip",
                        "VisTrailsPlugin-4.4.0-Qt4-Windows-64bit.zip"],
            "win32"  : [ "AcuSolveReaderPlugin-4.4.0-Qt4-Windows-32bit.zip",
                         "VisTrailsPlugin-4.4.0-Qt4-Windows-32bit.zip"],
          "linux64": [ "AcuSolveReaderPlugin-4.4.0-Qt4-Linux-64bit.tgz",
                       "VisTrailsPlugin-4.4.0-Qt4-Linux-64bit.tgz"],
            "osx"    : [ "VisTrailsPlugin-4.4.0-Qt4-OSX10.7-64bit.tgz"]},
  "catalyst" : {
              "all"  : [
                "Catalyst-base-v4.4.0.tar.gz",
                "Catalyst-base+python-v4.4.0.tar.gz",
                "Catalyst-base+essentials-v4.4.0.tar.gz",
                "Catalyst-base+essentials+python-v4.4.0.tar.gz",
                "Catalyst-base+essentials+extras-v4.4.0.tar.gz",
                "Catalyst-base+essentials+extras+python-v4.4.0.tar.gz",
                "Catalyst-base+essentials+extras+renderingbase-v4.4.0.tar.gz",
                "Catalyst-base+essentials+extras+renderingbase+python-v4.4.0.tar.gz" ]
               },
//  "app"    : {
//            "win64"  : [ "VeloView-3.1.1-26022015-Windows-64bit.exe"],
//            "win32"  : [ "VeloView-3.1.1-26022015-Windows-32bit.exe"],
//            "osx"    : [ "VeloView-3.1.1-26022015-Darwin-64bit.dmg"]
//             }
});


// ----------------------------------------------------------------------------
// ParaView 4.3.1
// ----------------------------------------------------------------------------

updateDynamicOptionList( "v4.3", {
  "binary" : {
            "win64"   : [ "ParaView-4.3.1-Windows-64bit.exe",
                          "ParaView-4.3.1-Windows-64bit.zip"],

            "win32"   : [ "ParaView-4.3.1-Windows-32bit.exe",
                          "ParaView-4.3.1-Windows-32bit.zip"],

            "linux64" : [ "ParaView-4.3.1-Linux-64bit-glibc-2.3.6.tar.gz" ],
            "osx"     : [ "ParaView-4.3.1-Darwin-64bit.dmg",
                          "ParaView-4.3.1-Darwin-64bit-SnowLeopard.dmg"]
             },
  "source" : {
            "all"     : [ "ParaView-v4.3.1-source.tar.gz",
                          "ParaView-v4.3.1-source.zip"]},
  "data"   : {
             "all"   : [ /*"ParaViewData-v4.1.0.zip",
                           "ParaViewData-v4.1.0.tar.gz",
                           "ParaViewManual.v4.0.pdf",*/
                        "TheParaViewGuide-v4.3-CC-Edition.pdf",
                        "ParaView-v4.3.1-API-doc.tar.gz",
			"ParaViewCatalystUsersGuide_v2.pdf"
                       ]},
  "plugin" : {
            "win64"  : ["AcuSolveReaderPlugin-4.3.1-Windows-64bit.zip",
                        "VisTrailsPlugin-4.3.1-Windows-64bit.zip"],
            "win32"  : [ "AcuSolveReaderPlugin-4.3.1-Windows-32bit.zip",
                         "VisTrailsPlugin-4.3.1-Windows-32bit.zip"],
            "linux64": [ "AcuSolveReaderPlugin-4.3.1-Linux-64bit-glibc-2.3.6.tgz",
                         "VisTrailsPlugin-4.3.1-Linux-64bit-glibc-2.3.6.tgz"],
            "osx"    : [ "VisTrailsPlugin-4.3.1-Darwin-64bit.tgz",
                         "VisTrailsPlugin-4.3.1-Darwin-64bit-SnowLeopard.tgz"]},
  "catalyst" : {
              "all"  : [
                "Catalyst-base-Source.tar.gz",
                "Catalyst-base+python-Source.tar.gz",
                "Catalyst-base+essentials-Source.tar.gz",
                "Catalyst-base+essentials+python-Source.tar.gz",
                "Catalyst-base+essentials+extras-Source.tar.gz",
                "Catalyst-base+essentials+extras+python-Source.tar.gz",
                "Catalyst-base+essentials+extras+renderingbase-Source.tar.gz",
                "Catalyst-base+essentials+extras+renderingbase+python-Source.tar.gz" ]
               },
  "app"    : {
            "win64"  : [ "VeloView-3.1.1-26022015-Windows-64bit.exe"],
            "win32"  : [ "VeloView-3.1.1-26022015-Windows-32bit.exe"],
            "osx"    : [ "VeloView-3.1.1-26022015-Darwin-64bit.dmg"]
             }
});

// ----------------------------------------------------------------------------
// ParaView 4.2.0
// ----------------------------------------------------------------------------

updateDynamicOptionList( "v4.2", {
  "binary" : {
            "win64"   : [ "ParaView-4.2.0-Windows-64bit.exe",
                          "ParaView-4.2.0-Windows-64bit.zip"],

            "win32"   : [ "ParaView-4.2.0-Windows-32bit.exe",
                          "ParaView-4.2.0-Windows-32bit.zip"],

            "linux64" : [ "ParaView-4.2.0-Linux-64bit.tar.gz" ],
            "osx"     : [ "ParaView-4.2.0-Darwin-64bit.dmg",
                          "ParaView-4.2.0-Darwin-64bit-SnowLeopard.dmg"]
             },
  "source" : {
            "all"     : [ "ParaView-v4.2.0-source.tar.gz",
                          "ParaView-v4.2.0-source.zip"]},
  "data"   : {
             "all"   : [ /*"ParaViewData-v4.1.0.zip",
                           "ParaViewData-v4.1.0.tar.gz",
                           "ParaViewManual.v4.0.pdf",*/
                        "ParaView-v4.2.0-API-doc.tar.gz"
                       ]},
  "plugin" : {
            "win64"  : ["AcuSolveReaderPlugin-4.2.0-Windows-64bit.tgz",
                        "VisTrailsPlugin-4.2.0-Windows-64bit.tgz"],
            "win32"  : [ "AcuSolveReaderPlugin-4.2.0-Windows-32bit.tgz",
                         "VisTrailsPlugin-4.2.0-Windows-32bit.tgz" ],
            "linux64": [ "AcuSolveReaderPlugin-4.2.0-Linux-64bit.tgz",
                         "VisTrailsPlugin-4.2.0-Linux-64bit.tgz"],
            "osx"    : [ "VisTrailsPlugin-4.2.0-Darwin-64bit.tgz",
                         "VisTrailsPlugin-4.2.0-Darwin-64bit-SnowLeopard.tgz"]},
  "catalyst" : {
              "all"  : [
                "Catalyst-base-Source.tar.gz",
                "Catalyst-base+python-Source.tar.gz",
                "Catalyst-base+essentials-Source.tar.gz",
                "Catalyst-base+essentials+python-Source.tar.gz",
                "Catalyst-base+essentials+extras-Source.tar.gz",
                "Catalyst-base+essentials+extras+python-Source.tar.gz",
                "Catalyst-base+essentials+extras+renderingbase-Source.tar.gz",
                "Catalyst-base+essentials+extras+renderingbase+python-Source.tar.gz" ]
               },
  /*
  "app"    : {
            "win64"  : [ "VeloView-2.0.0-31032014-Windows-64bit.exe"],
            "win32"  : [ "VeloView-2.0.0-31032014-Windows-32bit.exe"],
            "osx"    : [ "VeloView-2.0.0-31032014-Darwin-64bit.dmg"]
             }
  */
});

// ----------------------------------------------------------------------------
// ParaView 4.1.0
// ----------------------------------------------------------------------------

updateDynamicOptionList( "v4.1", {
  "binary" : {
            "win64"   : [ "ParaView-4.1.0-Windows-64bit.exe",
                          "ParaView-4.1.0-Windows-64bit.zip"],

            "win32"   : [ "ParaView-4.1.0-Windows-32bit.exe",
                          "ParaView-4.1.0-Windows-32bit.zip"],

            "linux64" : [ "ParaView-4.1.0-Linux-64bit-glibc-2.3.6.tar.gz" ],
            "linux32" : [ "ParaView-4.1.0-Linux-32bit-glibc-2.3.6.tar.gz"],
            "osx"     : [ "ParaView-4.1.0-Darwin-64bit.dmg",
                          "ParaView-4.1.0-Darwin-64bit-Lion-Python27.dmg"]
             },
  "source" : {
            "all"     : [ "ParaView-v4.1.0-source.tar.gz",
                          "ParaView-v4.1.0-source.zip"]},
  "data"   : {
             "all"   : [ "ParaViewData-v4.1.0.zip",
                         "ParaViewData-v4.1.0.tar.gz",
//                         "ParaViewManual.v4.0.pdf",
                         "ParaView-API-docs-v4.1.zip",
                         "ParaView-API-docs-v4.1.tar.gz"
                       ]},
  "plugin" : {
            "win64"  : ["AcuSolveReaderPlugin-4.1.0-Windows-64bit.tgz",
                        "VisTrailsPlugin-4.1.0-Windows-64bit.tgz"],
            "win32"  : [ "AcuSolveReaderPlugin-4.1.0-Windows-32bit.tgz",
                         "VisTrailsPlugin-4.1.0-Windows-32bit.tgz" ],
            "linux64": [ "AcuSolveReaderPlugin-4.1.0-Linux-64bit-glibc-2.3.6.tgz",
                         "VisTrailsPlugin-4.1.0-Linux-64bit-glibc-2.3.6.tgz"],
            "linux32": [ "AcuSolveReaderPlugin-4.1.0-Linux-32bit-glibc-2.3.6.tgz",
                         "VisTrailsPlugin-4.1.0-Linux-32bit-glibc-2.3.6.tgz"],
            "osx"    : [ "VisTrailsPlugin-4.1.0-Darwin-64bit.tgz",
                         "VisTrailsPlugin-4.1.0-Darwin-64bit-Lion-Python27.tgz"]},
  "catalyst" : {
              "all"  : [ "Catalyst-base-Source.tar.gz",
                         "Catalyst-base+essentials-Source.tar.gz",
                         "Catalyst-base+essentials+extras-Source.tar.gz",
                         "Catalyst-base+python-Source.tar.gz",
                         "Catalyst-base+essentials+python-Source.tar.gz",
                         "Catalyst-base+essentials+extras+python-Source.tar.gz"]
               },
  "app"    : {
            "win64"  : [ "VeloView-2.0.0-31032014-Windows-64bit.exe"],
            "win32"  : [ "VeloView-2.0.0-31032014-Windows-32bit.exe"],
            "osx"    : [ "VeloView-2.0.0-31032014-Darwin-64bit.dmg"]
             }
});

// ----------------------------------------------------------------------------
// ParaView 4.0.1
// ----------------------------------------------------------------------------

updateDynamicOptionList( "v4.0", {
  "binary" : {
            "win64"   : [ "ParaView-4.0.1-Windows-64bit.exe",
                          "ParaView-4.0.1-Windows-64bit.zip" ],
            "win32"   : [ "ParaView-4.0.1-Windows-32bit.exe",
                          "ParaView-4.0.1-Windows-32bit.zip"],
            "linux64" : [ "ParaView-4.0.1-Linux-64bit.tar.gz" ],
            "linux32" : [ "ParaView-4.0.1-Linux-32bit.tar.gz"],
		"osx" : [ "ParaView-4.0.1-Darwin-64bit.dmg",
			  "ParaView-4.0.1-Lion-Python27-64bit.dmg"]},
  "source" : {
            "all"     : [ "ParaView-v4.0.1-source.tgz",
			  "ParaView-v4.0.1-source.zip"]},
  "data"   : {
             "all"   : [ "ParaViewData-v4.0.1.zip",
                         "ParaViewData-v4.0.1.tar.gz",
                         "ParaViewManual.v4.0.pdf",
                         "ParaView-doc-api.tar.gz"]},
  "plugin" : {
            "win64"  : [ "AcuSolveReaderPlugin-4.0.1-Windows-64bit.tgz",
                         "VisTrailsPlugin-4.0.1-Windows-64bit.tgz"],
            "win32"  : [ "VisTrailsPlugin-4.0.1-Windows-32bit.tgz",
                         "AcuSolveReaderPlugin-4.0.1-Windows-32bit.tgz"],
            "linux64": [ "AcuSolveReaderPlugin-4.0.1-Linux-64bit.tgz",
                         "CosmologyToolsPlugin-4.0.1-Linux-64bit.tgz",
                         "VisTrailsPlugin-4.0.1-Linux-64bit.tgz"],
            "linux32": [ "AcuSolveReaderPlugin-4.0.1-Linux-32bit.tgz",
                         "CosmologyToolsPlugin-4.0.1-Linux-32bit.tgz",
                         "VisTrailsPlugin-4.0.1-Linux-32bit.tgz"],
            "osx"    : [ "CosmologyToolsPlugin-4.0.1-Darwin-64bit.tgz",
                         "VisTrailsPlugin-4.0.1-Darwin-64bit.tgz",
			 "CosmologyToolsPlugin-4.0.1-Lion-Python27-64bit.tgz",
                         "VisTrailsPlugin-4.0.1-Lion-Python27-64bit.tgz"]},
  "app"    : {
            "win64"  : [ "VeloView-1.0.8-Windows-64bit.exe"],
            "win32"  : [ "VeloView-1.0.8-Windows-32bit.exe"],
            "osx"    : [ "VeloView-1.0.8-Darwin-64bit.dmg"]
             }
});

// ----------------------------------------------------------------------------
// ParaView 3.98.1
// ----------------------------------------------------------------------------

updateDynamicOptionList( "v3.98", {
  "binary" : {
            "win64"   : [ "ParaView-3.98.1-Windows-64bit.exe",
                          "ParaView-3.98.1-Windows-64bit.zip",
                          "ParaView-3.98.0-Windows-64bit.exe",
                          "ParaView-3.98.0-Windows-64bit.zip" ],
            "win32"   : [ "ParaView-3.98.1-Windows-32bit.exe",
                          "ParaView-3.98.1-Windows-32bit.zip",
                          "ParaView-3.98.0-Windows-32bit.exe",
                          "ParaView-3.98.0-Windows-32bit.zip" ],
            "linux64" : [ "ParaView-3.98.1-Linux-64bit.tar.gz",
                          "ParaView-3.98.0-Linux-64bit-glibc-2.3.6.tar.gz" ],
            "linux32" : [ "ParaView-3.98.1-Linux-32bit.tar.gz",
                          "ParaView-3.98.0-Linux-32bit-glibc-2.3.6.tar.gz" ],
            "osx"     : [ "ParaView-3.98.1-Darwin-64bit.dmg",
                          "ParaView-3.98.0-OSX-10.6-10.7-64bit.dmg",
                          "ParaView-3.98.0-OSX-10.8-64bit.dmg" ]},
  "source" : {
            "all"     : [ "ParaView-3.98.1-source.tar.gz",
                          "ParaView-3.98.1-source.zip",
                          "ParaView-3.98.0-src.tgz",
                          "ParaView-3.98.0-src.zip" ]},
  "data" : {
            "all"     : [ "ParaViewData-3.98.1.tar.gz",
                          "ParaViewData-3.98.1.zip",
                          "ParaViewData.zip",
                          "ParaViewManual.v3.98.pdf" ]},
  "plugin" : {
            "win64"  : [ "AcuSolveReaderPlugin-3.98.1-Windows-64bit.zip",
                         "VisTrailsPlugin-3.98.1-Windows-64bit.zip",
                         "AcuSolveReaderPlugin-Windows-64bit.tgz",
                         "VisTrailsPlugin-Windows-64bit.tgz" ],
            "win32"  : [ "AcuSolveReaderPlugin-3.98.1-Windows-32bit.zip",
                         "VisTrailsPlugin-3.98.1-Windows-32bit.zip",
                         "AcuSolveReaderPlugin-Windows-32bit.tgz",
                         "VisTrailsPlugin-Windows-32bit.tgz"],
            "linux64": [ "AcuSolveReaderPlugin-3.98.1-Linux-64bit.tar.gz",
                         "CosmologyToolsPlugin-3.98.1-Linux-64bit.tar.gz",
                         "VisTrailsPlugin-3.98.1-Linux-64bit.tar.gz",
                         "CosmologyToolsPlugin-Linux-64bit-glibc-2.3.6.tgz",
                         "CosmologyToolsPlugin-Linux-64bit-glibc-2.15.tgz",
                         "AcuSolveReaderPlugin-Linux-64bit-glibc-2.3.6.tgz",
                         "AcuSolveReaderPlugin-Linux-64bit-glibc-2.15.tgz",
                         "VisTrailsPlugin-Linux-64bit-glibc-2.3.6.tgz",
                         "VisTrailsPlugin-Linux-64bit-glibc-2.15.tgz"],
            "linux32": [ "AcuSolveReaderPlugin-3.98.1-Linux-32bit.tar.gz",
                         "CosmologyToolsPlugin-3.98.1-Linux-32bit.tar.gz",
                         "VisTrailsPlugin-3.98.1-Linux-32bit.tar.gz",
                         "CosmologyToolsPlugin-Linux-32bit-glibc-2.3.6.tgz",
                         "AcuSolveReaderPlugin-Linux-32bit-glibc-2.3.6.tgz"],
            "osx"    : [ "CosmologyToolsPlugin-3.98.1-Darwin-64bit.tar.gz",
                         "VisTrailsPlugin-3.98.1-Darwin-64bit.tar.gz",
                         "CosmologyToolsPlugin-OSX-10.6-10.7-64bit.tgz"]}
});

// ----------------------------------------------------------------------------
// ************* PARAVIEW VERSION 3.14 *************************************
// ----------------------------------------------------------------------------
whichVersion.forValue('v3.14').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.14').addOptionsTextValue('ParaView Source Files', 'source');
whichVersion.forValue('v3.14').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v3.14').addOptionsTextValue('Community Contributed Plugins', 'plugin');whichVersion.forValue('v3.14').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.14').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.14').forValue('binary').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.14').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.14').forValue('binary').addOptionsTextValue('Mac OSX 64-bit Intel', 'Darwin-x86_64');whichVersion.forValue('v3.14').forValue('data').addOptionsTextValue('All', 'all');// ParaView Binaries
whichVersion.forValue('v3.14').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-3.14.1-Darwin-x86_64.dmg');
whichVersion.forValue('v3.14').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.14.1-Linux-64bit.tar.gz');
whichVersion.forValue('v3.14').forValue('binary').forValue('Linux-x86').addOptions('ParaView-3.14.1-Linux-32bit.tar.gz');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win32').addOptions('ParaView-3.14.1-Win32-x86.exe');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win32').addOptions('ParaView-3.14.1-Win32-x86.zip');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win64').addOptions('ParaView-3.14.1-Win64-x86.exe');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win64').addOptions('ParaView-3.14.1-Win64-x86.zip');whichVersion.forValue('v3.14').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-3.14.0-Darwin-x86_64.dmg');
whichVersion.forValue('v3.14').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.14.0-Linux-64bit.tar.gz');
whichVersion.forValue('v3.14').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.14.1-with-PVMeshless-Linux-64bit.tar.gz');
whichVersion.forValue('v3.14').forValue('binary').forValue('Linux-x86').addOptions('ParaView-3.14.0-Linux-32bit.tar.gz');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win32').addOptions('ParaView-3.14.0-Win32-x86.exe');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win32').addOptions('ParaView-3.14.0-Win32-x86.zip');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win64').addOptions('ParaView-3.14.0-Win64-x86.exe');
whichVersion.forValue('v3.14').forValue('binary').forValue('Win64').addOptions('ParaView-3.14.0-Win64-x86.zip');// ParaView Data
whichVersion.forValue('v3.14').forValue('binary').forValue('Win64').addOptions('ParaView-3.14.1-with-PVMeshless-Windows-64bit.zip');
whichVersion.forValue('v3.14').forValue('data').forValue('all').addOptions('ParaViewData-3.14.0.tar.gz');
whichVersion.forValue('v3.14').forValue('data').forValue('all').addOptions('ParaViewUsersGuide.v3.14.pdf');// ParaView Sources
whichVersion.forValue('v3.14').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.14').forValue('source').forValue('all').addOptions('ParaView-3.14.1-Source.tar.gz');
whichVersion.forValue('v3.14').forValue('source').forValue('all').addOptions('ParaView-3.14.1-WinSource.zip');
whichVersion.forValue('v3.14').forValue('source').forValue('all').addOptions('ParaView-3.14.0-Source.tar.gz');
whichVersion.forValue('v3.14').forValue('source').forValue('all').addOptions('ParaView-3.14.0-WinSource.zip');// ParaView Plugin categories
whichVersion.forValue('v3.14').forValue('plugin').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.14').forValue('plugin').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.14').forValue('plugin').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.14').forValue('plugin').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.14').forValue('plugin').addOptionsTextValue('Mac OSX 64-bit Intel', 'Darwin-x86_64');// ParaView Plugins
whichVersion.forValue('v3.14').forValue('plugin').forValue('Darwin-x86_64').addOptions('VisTrailsPlugin-Darwin-x86_64.tgz');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Linux-x86_64').addOptions('AcuSolveReaderPlugin-Linux-64bit.tgz');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Linux-x86_64').addOptions('VisTrailsPlugin-Linux-64bit.tgz');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Linux-x86').addOptions('AcuSolveReaderPlugin-Linux-32bit.tgz');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Linux-x86').addOptions('VisTrailsPlugin-Linux-32bit.tgz');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Win32').addOptions('AcuSolveReaderPlugin-Windows-32bit.zip');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Win32').addOptions('VisTrailsPlugin-Windows-32bit.zip');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Win64').addOptions('AcuSolveReaderPlugin-Windows-64bit.zip');
whichVersion.forValue('v3.14').forValue('plugin').forValue('Win64').addOptions('VisTrailsPlugin-Windows-64bit.zip');// ************* PARAVIEW VERSION 3.12 *************************************
whichVersion.forValue('v3.12').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.12').addOptionsTextValue('ParaView Source Files', 'source');
whichVersion.forValue('v3.12').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v3.12').addOptionsTextValue('Community Contributed Plugins', 'plugin');whichVersion.forValue('v3.12').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.12').forValue('source').forValue('all').addOptions('ParaView-3.12.0.zip');
whichVersion.forValue('v3.12').forValue('source').forValue('all').addOptions('ParaView-3.12.0.tar.gz');whichVersion.forValue('v3.12').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.12').forValue('data').forValue('all').addOptions('ParaViewData-3.12.0.zip');
whichVersion.forValue('v3.12').forValue('data').forValue('all').addOptions('ParaView Users Guide v3.12.pdf');whichVersion.forValue('v3.12').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.12').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.12').forValue('binary').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.12').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.12').forValue('binary').addOptionsTextValue('Mac OSX 64-bit Intel', 'Darwin-x86_64');whichVersion.forValue('v3.12').forValue('binary').forValue('Win64').addOptions('ParaView-3.12.0-Win64-x86.exe');
whichVersion.forValue('v3.12').forValue('binary').forValue('Win32').addOptions('ParaView-3.12.0-Win32-x86.exe');
whichVersion.forValue('v3.12').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.12.0-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.12').forValue('binary').forValue('Linux-x86').addOptions('ParaView-3.12.0-Linux-i686.tar.gz');
whichVersion.forValue('v3.12').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-3.12.0-Darwin-x86_64.dmg');whichVersion.forValue('v3.12').forValue('plugin').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.12').forValue('plugin').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.12').forValue('plugin').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.12').forValue('plugin').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.12').forValue('plugin').addOptionsTextValue('Mac OSX 64-bit Intel', 'Darwin-x86_64');whichVersion.forValue('v3.12').forValue('plugin').forValue('Win32').addOptions('AcuSolveReaderPlugin-win32-x86.zip');
whichVersion.forValue('v3.12').forValue('plugin').forValue('Win64').addOptions('AcuSolveReaderPlugin-win64-x86.zip');
whichVersion.forValue('v3.12').forValue('plugin').forValue('Linux-x86_64').addOptions('AcuSolveReaderPlugin-linux-x86_64.tar.gz');
whichVersion.forValue('v3.12').forValue('plugin').forValue('Linux-x86').addOptions('AcuSolveReaderPlugin-linux-i686.tar.gz');
whichVersion.forValue('v3.12').forValue('plugin').forValue('Linux-x86_64').addOptions('pvNektarReader-3.12.0-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.12').forValue('plugin').forValue('Linux-x86').addOptions('pvNektarReader-3.12.0-Linux-i686.tar.gz');// ************* PARAVIEW VERSION 3.11 *************************************
//whichVersion.forValue('v3.11').addOptionsTextValue('ParaView Binary Installers', 'binary');
//whichVersion.forValue('v3.11').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
//whichVersion.forValue('v3.11').forValue('binary').forValue('Win32').addOptions('ParaView-3.11.1-Win32-x86.exe');
//whichVersion.forValue('v3.11').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
//whichVersion.forValue('v3.11').forValue('binary').forValue('Win64').addOptions('ParaView-3.11.1-Win64-x86.exe');
//whichVersion.forValue('v3.11').addOptionsTextValue('Source Code', 'source');
//whichVersion.forValue('v3.11').forValue('source').addOptionsTextValue('All', 'all');
//whichVersion.forValue('v3.11').forValue('source').forValue('all').addOptions('ParaView-3.11.1.tar.gz');// ************* PARAVIEW VERSION 3.10.1 *************************************
whichVersion.forValue('v3.10').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.10').forValue('binary').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.10').forValue('binary').forValue('Linux-x86_64').addOptions('ParaViewServers-3.10.1-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.10').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v3.10').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.10').forValue('source').forValue('all').addOptions('ParaView-3.10.1.tar.gz');
whichVersion.forValue('v3.10').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.10').forValue('binary').forValue('Win32').addOptions('ParaView-3.10.1-Win32-x86.exe');
whichVersion.forValue('v3.10').forValue('source').forValue('all').addOptions('ParaView-3.10.1-OSX-10.7-x86_64.dmg');
whichVersion.forValue('v3.10').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.10').forValue('binary').forValue('Win64').addOptions('ParaView-3.10.1-Win64-x86.exe');
whichVersion.forValue('v3.10').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.10.1-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.10').forValue('binary').addOptionsTextValue('Mac OSX 64-bit Intel', 'Darwin-x86_64');
whichVersion.forValue('v3.10').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-3.10.1-Darwin-x86_64.dmg');
whichVersion.forValue('v3.10').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.10').forValue('binary').forValue('Linux-x86').addOptions('ParaView-3.10.1-Linux-i686.tar.gz');
whichVersion.forValue('v3.10').forValue('source').forValue('all').addOptions('ParaView-3.10.1.zip');
whichVersion.forValue('v3.10').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v3.10').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.10').forValue('data').forValue('all').addOptions('ParaViewData-3.10.1.zip');
whichVersion.forValue('v3.10').forValue('data').forValue('all').addOptions('ParaViewData-3.10.0.zip');
whichVersion.forValue('v3.10').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.10.0-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.10').forValue('source').forValue('all').addOptions('ParaView-3.10.0.tar.gz');
whichVersion.forValue('v3.10').forValue('source').forValue('all').addOptions('ParaView-3.10.0.zip');
whichVersion.forValue('v3.10').forValue('binary').forValue('Win64').addOptions('ParaView-3.10.0-Win64-x86.exe');
whichVersion.forValue('v3.10').forValue('binary').forValue('Linux-x86').addOptions('ParaView-3.10.0-Linux-i686.tar.gz');
whichVersion.forValue('v3.10').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-3.10.0-Darwin-x86_64.dmg');
whichVersion.forValue('v3.10').forValue('binary').forValue('Win32').addOptions('ParaView-3.10.0-Win32-x86.exe');
whichVersion.forValue('v3.9').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.9').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.9').forValue('binary').forValue('Win32').addOptions('ParaView-3.9.0-Win32-x86.exe');
whichVersion.forValue('v3.9').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.9').forValue('binary').forValue('Win64').addOptions('ParaView-3.9.0-Win64-x86.exe');
whichVersion.forValue('v3.8').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v3.8').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.8').forValue('data').forValue('all').addOptions('ParaViewData-3.8.1.tar.gz');
whichVersion.forValue('v3.8').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.8').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win64').addOptions('ParaView-3.8.1-Win64-x86.exe');
whichVersion.forValue('v3.8').forValue('data').forValue('all').addOptions('ParaViewData-3.8.1.zip');
whichVersion.forValue('v3.8').forValue('binary').addOptionsTextValue('Mac OSX Universal Binaries', 'Darwin-Universal');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-Universal').addOptions('ParaViewServers-3.8.1-Darwin-Universal.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').addOptionsTextValue('Mac OSX 64-bit Intel', 'Darwin-x86_64');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-3.8.1-Darwin-x86_64.dmg');
whichVersion.forValue('v3.8').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86').addOptions('ParaView-3.8.1-Linux-i686.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.8.1-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaViewServers-3.8.1-Darwin-x86_64.tar.gz');
whichVersion.forValue('v3.8').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v3.8').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.8').forValue('source').forValue('all').addOptions('ParaView-3.8.1.zip');
whichVersion.forValue('v3.8').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win32').addOptions('ParaView-3.8.1-Win32-x86.exe');
whichVersion.forValue('v3.8').forValue('source').forValue('all').addOptions('ParaView-3.8.1.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-Universal').addOptions('ParaView-3.8.1-Darwin-Universal.dmg');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86').addOptions('ParaView-Development-3.8.1-Linux-i686.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-Development-3.8.1-Darwin-x86_64.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-Universal').addOptions('ParaView-Development-3.8.1-Darwin-Universal.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win64').addOptions('ParaView-Development-3.8.1-Win64-x86.exe');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win32').addOptions('ParaView-Development-3.8.1-Win32-x86.exe');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-Development-3.8.1-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-3.8.0-Darwin-x86_64.dmg');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-3.8.0-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-Universal').addOptions('ParaViewServers-3.8.0-Darwin-Universal.tar.gz');
whichVersion.forValue('v3.8').forValue('source').forValue('all').addOptions('ParaView-3.8.0.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86').addOptions('ParaView-3.8.0-Linux-i686.tar.gz');
whichVersion.forValue('v3.8').forValue('data').forValue('all').addOptions('ParaViewData-3.8.0.zip');
whichVersion.forValue('v3.8').forValue('source').forValue('all').addOptions('ParaView-3.8.0.zip');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win32').addOptions('ParaView-3.8.0-Win32-x86.exe');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-Universal').addOptions('ParaView-3.8.0-Darwin-Universal.dmg');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win64').addOptions('ParaView-3.8.0-Win64-x86.exe');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaViewServers-3.8.0-Darwin-x86_64.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-x86_64').addOptions('ParaView-Development-3.8.0-Darwin-x86_64.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86_64').addOptions('ParaView-Development-3.8.0-Linux-x86_64.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Darwin-Universal').addOptions('ParaView-Development-3.8.0-Darwin-Universal.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Linux-x86').addOptions('ParaView-Development-3.8.0-Linux-i686.tar.gz');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win64').addOptions('ParaView-Development-3.8.0-Win64-x86.exe');
whichVersion.forValue('v3.8').forValue('binary').forValue('Win32').addOptions('ParaView-Development-3.8.0-Win32-x86.exe');
whichVersion.forValue('v3.6').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.6').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.6').forValue('binary').forValue('Linux-x86').addOptions('paraview-3.6.2-Linux32-x86.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').addOptionsTextValue('Mac OSX Universal Binaries', 'Darwin-Universal');
whichVersion.forValue('v3.6').forValue('binary').forValue('Darwin-Universal').addOptions('paraview.cl-3.6.2-universal.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.6').forValue('binary').forValue('Linux-x86_64').addOptions('paraview-3.6.2-Linux64-x86.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').forValue('Darwin-Universal').addOptions('paraview.app-3.6.2-universal.dmg');
whichVersion.forValue('v3.6').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v3.6').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.6').forValue('source').forValue('all').addOptions('paraview-3.6.2.zip');
whichVersion.forValue('v3.6').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.6').forValue('binary').forValue('Win32').addOptions('paraview-3.6.2-win32-x86.exe');
whichVersion.forValue('v3.6').forValue('source').forValue('all').addOptions('paraview-3.6.2.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.6').forValue('binary').forValue('Win64').addOptions('paraview-3.6.2-win64-x86.exe');
whichVersion.forValue('v3.6').forValue('binary').addOptionsTextValue('Mac OSX v10.4', 'Darwin-Tiger');
whichVersion.forValue('v3.6').forValue('binary').forValue('Darwin-Tiger').addOptions('paraview.cl-3.6.1-Tiger-x86.tar.gz');
whichVersion.forValue('v3.6').forValue('source').forValue('all').addOptions('paraview-3.6.1.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').forValue('Darwin-Tiger').addOptions('paraview.app-3.6.1-Tiger-x86.dmg');
whichVersion.forValue('v3.6').forValue('source').forValue('all').addOptions('paraview-3.6.1.zip');
whichVersion.forValue('v3.6').forValue('binary').forValue('Darwin-Tiger').addOptions('paraview.cl-3.6.1-Tiger-ppc.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').forValue('Darwin-Tiger').addOptions('paraview.app-3.6.1-Tiger-ppc.dmg');
whichVersion.forValue('v3.6').forValue('binary').forValue('Win32').addOptions('paraview-3.6.1-win32-x86.exe');
whichVersion.forValue('v3.6').forValue('binary').forValue('Linux-x86_64').addOptions('paraview-3.6.1-Linux64-x86.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').forValue('Linux-x86').addOptions('paraview-3.6.1-Linux32-x86.tar.gz');
whichVersion.forValue('v3.6').forValue('binary').forValue('Win64').addOptions('paraview-3.6.1-win64-x86.exe');
whichVersion.forValue('v3.4').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v3.4').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.4').forValue('source').forValue('all').addOptions('paraview-3.4.0.tar.gz');
whichVersion.forValue('v3.4').forValue('source').forValue('all').addOptions('paraview-3.4.0.zip');
whichVersion.forValue('v3.4').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.4').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.4').forValue('binary').forValue('Win32').addOptions('paraview-3.4.0-win32-x86.exe');
whichVersion.forValue('v3.4').forValue('binary').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.4').forValue('binary').forValue('Win64').addOptions('paraview-3.4.0-win64-x86.exe');
whichVersion.forValue('v3.2').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v3.2').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('paraview-3.2.3.zip');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('paraview-3.2.3.tar.gz');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('paraview-3.2.2.zip');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('paraview-3.2.2.tar.gz');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('paraview-3.2.1.zip');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('paraview-3.2.1.tar.gz');
whichVersion.forValue('v3.2').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.2').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.2').forValue('binary').forValue('Win32').addOptions('paraview-3.2.1-win32-x86.exe');
whichVersion.forValue('v3.2').forValue('binary').forValue('Win32').addOptions('paraview-3.2.0-win32-x86.exe');
whichVersion.forValue('v3.1').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.1').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.1').forValue('binary').forValue('Win32').addOptions('paraview-3.1.0-win32-x86.exe');
whichVersion.forValue('v3.0').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v3.0').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.0').forValue('binary').forValue('Win32').addOptions('paraview-3.0.2-win32-x86.exe');
whichVersion.forValue('v3.0').forValue('binary').forValue('Win32').addOptions('paraview-3.0.1-win32-x86.exe');
whichVersion.forValue('v3.0').forValue('binary').forValue('Win32').addOptions('paraview-3.0.0-win32-x86.exe');
whichVersion.forValue('v2.9').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v2.9').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.9-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.8-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.7-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.6-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.5-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.4-win32-x86.zip');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.4-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.3-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.2-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.1-win32-x86.exe');
whichVersion.forValue('v2.9').forValue('binary').forValue('Win32').addOptions('paraview-2.9.0-win32-x86.exe');
whichVersion.forValue('v2.6').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v2.6').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('paraview-2.6.2.zip');
whichVersion.forValue('v2.6').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v2.6').forValue('binary').addOptionsTextValue('Mac OSX Power PC', 'Darwin-PPC');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-PPC').addOptions('ParaView-2.6.2-darwin-ppc.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').addOptionsTextValue('Mac OSX x86 Intel', 'Darwin-x86');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-x86').addOptions('ParaView-2.6.2-darwin-intel.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v2.6').forValue('binary').forValue('Linux-x86').addOptions('ParaView-2.6.2-x86-linux.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-PPC').addOptions('ParaView-2.6.2-darwin-ppc.tar.gz');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-x86').addOptions('ParaView-2.6.2-darwin-intel.tar.gz');
whichVersion.forValue('v2.6').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v2.6').forValue('binary').forValue('Win32').addOptions('paraview-2.6.2-win32.exe');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('ParaView-2.6.2.tar.gz');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('ParaView-2.6.2.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').forValue('Linux-x86').addOptions('ParaView-2.6.2-x86-linux.tar.gz');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('ParaView-2.6.1.tar.gz');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('ParaView-2.6.1.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-PPC').addOptions('ParaView-2.6.1-darwin-ppc.tar.gz');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-x86').addOptions('ParaView-2.6.1-darwin-intel.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').forValue('Win32').addOptions('paraview-2.6.1-win32.exe');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-x86').addOptions('ParaView-2.6.1-darwin-intel.tar.gz');
whichVersion.forValue('v2.6').forValue('binary').forValue('Linux-x86').addOptions('ParaView-2.6.1-x86-linux.tar.Z');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('paraview-2.6.1.zip');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-PPC').addOptions('ParaView-2.6.1-darwin-ppc.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').forValue('Linux-x86').addOptions('ParaView-2.6.1-x86-linux.tar.gz');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('paraview-2.6.0.zip');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-x86').addOptions('ParaView-2.6.0-darwin-intel.tar.gz');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('ParaView-2.6.0.tar.gz');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-x86').addOptions('ParaView-2.6.0-darwin.tar.gz');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-PPC').addOptions('ParaView-2.6.0-darwin-ppc.tar.gz');
whichVersion.forValue('v2.6').forValue('binary').forValue('Linux-x86').addOptions('ParaView-2.6.0-x86-linux.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').forValue('Darwin-x86').addOptions('ParaView-2.6.0-darwin.tar.Z');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('ParaView-2.6.0.tar.Z');
whichVersion.forValue('v2.6').forValue('binary').forValue('Win32').addOptions('paraview-2.6.0-win32.exe');
whichVersion.forValue('v2.6').forValue('binary').forValue('Linux-x86').addOptions('ParaView-2.6.0-x86-linux.tar.gz');
whichVersion.forValue('v2.5').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v2.5').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v2.5').forValue('binary').forValue('Win32').addOptions('paraview-2.5.0-win32.exe');
whichVersion.forValue('v2.4').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v2.4').forValue('binary').addOptionsTextValue('Mac OSX x86 Intel', 'Darwin-x86');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.4-darwin.tar.Z');
whichVersion.forValue('v2.4').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v2.4').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.4.zip');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.4.tar.Z');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.4-darwin.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v2.4').forValue('binary').forValue('Win32').addOptions('paraview-2.4.4-win32.exe');
whichVersion.forValue('v2.4').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.4-x86-linux.tar.Z');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.4.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.4-x86-linux.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Win32').addOptions('paraview-2.4.3-win32.exe');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.3-darwin.tar.Z');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.3.zip');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.3-x86-linux.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.3-x86-linux.tar.Z');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.3.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.3-darwin.tar.gz');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.3.tar.Z');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.2.tar.Z');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.2-x86-linux.tar.gz');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.2.zip');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.2.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.2-darwin.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.2-x86-linux.tar.Z');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.2-darwin.tar.Z');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.1.zip');
whichVersion.forValue('v2.4').forValue('binary').forValue('Win32').addOptions('paraview-2.4.1-win32.exe');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.1.tar.gz');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.1.tar.Z');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.1-darwin.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.1-x86-linux.tar.Z');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.1-darwin.tar.Z');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.1-x86-linux.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.0-darwin.tar.Z');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.0-x86-linux.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.4.0-darwin.tar.gz');
whichVersion.forValue('v2.4').forValue('binary').forValue('Win32').addOptions('paraview-2.4.0-win32.exe');
whichVersion.forValue('v2.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.4.0-x86-linux.tar.Z');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.0.tar.Z');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.0.tar.gz');
whichVersion.forValue('v2.4').forValue('source').forValue('all').addOptions('paraview-2.4.0.zip');
whichVersion.forValue('v2.2').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v2.2').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v2.2').forValue('binary').forValue('Win32').addOptions('paraview-2.2.1-win32.exe');
whichVersion.forValue('v2.2').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v2.2').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.2').forValue('source').forValue('all').addOptions('paraview-2.2.1.tar.gz');
whichVersion.forValue('v2.2').forValue('source').forValue('all').addOptions('paraview-2.2.1.zip');
whichVersion.forValue('v2.2').forValue('source').forValue('all').addOptions('paraview-2.2.1.tar.Z');
whichVersion.forValue('v2.2').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v2.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.2.1-x86-linux.tar.gz');
whichVersion.forValue('v2.2').forValue('binary').addOptionsTextValue('Mac OSX x86 Intel', 'Darwin-x86');
whichVersion.forValue('v2.2').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.2.1-darwin.tar.Z');
whichVersion.forValue('v2.2').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.2.1-darwin.tar.gz');
whichVersion.forValue('v2.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.2.1-x86-linux.tar.Z');
whichVersion.forValue('v2.2').forValue('binary').forValue('Win32').addOptions('paraview-2.2.0-win32.exe');
whichVersion.forValue('v2.2').forValue('source').forValue('all').addOptions('paraview-2.2.0.tar.gz');
whichVersion.forValue('v2.2').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.2.0-darwin.tar.gz');
whichVersion.forValue('v2.2').forValue('binary').forValue('Darwin-x86').addOptions('paraview-2.2.0-darwin.tar.Z');
whichVersion.forValue('v2.2').forValue('source').forValue('all').addOptions('paraview-2.2.0.tar.Z');
whichVersion.forValue('v2.2').forValue('source').forValue('all').addOptions('paraview-2.2.0.zip');
whichVersion.forValue('v2.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.2.0-x86-linux.tar.gz');
whichVersion.forValue('v2.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.2.0-x86-linux.tar.Z');
whichVersion.forValue('v2.0').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v2.0').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.3.zip');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.3.tar.Z');
whichVersion.forValue('v2.0').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v2.0').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.3-x86-linux.tar.Z');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.3-x86-linux.tar.gz');
whichVersion.forValue('v2.0').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v2.0').forValue('binary').forValue('Win32').addOptions('paraview-2.0.3-win32.exe');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.3.tar.gz');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.2-x86-linux.tar.Z');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.2.zip');
whichVersion.forValue('v2.0').forValue('binary').forValue('Win32').addOptions('paraview-2.0.2-win32.exe');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.2.tar.Z');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.2-x86-linux.tar.gz');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.2.tar.gz');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.1.tar.Z');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.1.zip');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.1-x86-linux.tar.gz');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.1-x86-linux.tar.Z');
whichVersion.forValue('v2.0').forValue('binary').forValue('Win32').addOptions('paraview-2.0.1-win32.exe');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.1.tar.gz');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.0-x86-linux.tar.Z');
whichVersion.forValue('v2.0').forValue('binary').forValue('Win32').addOptions('paraview-2.0.0-win32.exe');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.0.tar.Z');
whichVersion.forValue('v2.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-2.0.0-x86-linux.tar.gz');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.0.zip');
whichVersion.forValue('v2.0').forValue('source').forValue('all').addOptions('paraview-2.0.0.tar.gz');
whichVersion.forValue('v1.8').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v1.8').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.5.tar.Z');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.5.zip');
whichVersion.forValue('v1.8').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v1.8').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v1.8').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.8.5-x86-linux.tar.Z');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.5.tar.gz');
whichVersion.forValue('v1.8').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.8.5-x86-linux.tar.gz');
whichVersion.forValue('v1.8').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v1.8').forValue('binary').forValue('Win32').addOptions('paraview-1.8.5-win32.exe');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.4.zip');
whichVersion.forValue('v1.8').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.8.4-x86-linux.tar.gz');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.4.tar.gz');
whichVersion.forValue('v1.8').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.8.4-x86-linux.tar.Z');
whichVersion.forValue('v1.8').forValue('binary').forValue('Win32').addOptions('paraview-1.8.4-win32.exe');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.4.tar.Z');
whichVersion.forValue('v1.8').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.8.3-x86-linux.tar.Z');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.3.zip');
whichVersion.forValue('v1.8').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.8.3-x86-linux.tar.gz');
whichVersion.forValue('v1.8').forValue('binary').forValue('Win32').addOptions('paraview-1.8.3-win32.exe');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.3.tar.gz');
whichVersion.forValue('v1.8').forValue('source').forValue('all').addOptions('paraview-1.8.3.tar.Z');
whichVersion.forValue('v1.6').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v1.6').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v1.6').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.6.3-x86-linux.tar.Z');
whichVersion.forValue('v1.6').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v1.6').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.3.tar.gz');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.3.tar.Z');
whichVersion.forValue('v1.6').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.6.3-x86-linux.tar.gz');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.3.zip');
whichVersion.forValue('v1.6').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v1.6').forValue('binary').forValue('Win32').addOptions('paraview-1.6.3-win32.exe');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.2.tar.gz');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.2.tar.Z');
whichVersion.forValue('v1.6').forValue('binary').forValue('Win32').addOptions('paraview-1.6.2-win32.exe');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.2.zip');
whichVersion.forValue('v1.6').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.6.1-x86-linux.tar.Z');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.1.tar.Z');
whichVersion.forValue('v1.6').forValue('binary').forValue('Win32').addOptions('paraview-1.6.1-win32.exe');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.1.tar.gz');
whichVersion.forValue('v1.6').forValue('binary').addOptionsTextValue('Irix 64-bit', 'irix64');
whichVersion.forValue('v1.6').forValue('binary').forValue('irix64').addOptions('paraview-1.6.1-irix64.tar.Z');
whichVersion.forValue('v1.6').forValue('source').forValue('all').addOptions('paraview-1.6.1.zip');
whichVersion.forValue('v1.6').forValue('binary').forValue('irix64').addOptions('paraview-1.6.1-irix64.tar.gz');
whichVersion.forValue('v1.6').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.6.1-x86-linux.tar.gz');
whichVersion.forValue('v1.4').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v1.4').forValue('binary').addOptionsTextValue('HP Unix', 'hpux');
whichVersion.forValue('v1.4').forValue('binary').forValue('hpux').addOptions('paraview-1.4.3-hpux-static.tar.Z');
whichVersion.forValue('v1.4').forValue('binary').addOptionsTextValue('Irix 64-bit', 'irix64');
whichVersion.forValue('v1.4').forValue('binary').forValue('irix64').addOptions('paraview-1.4.3-irix64.tar.gz');
whichVersion.forValue('v1.4').forValue('binary').forValue('irix64').addOptions('paraview-1.4.3-irix64.tar.Z');
whichVersion.forValue('v1.4').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v1.4').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.3.tar.gz');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.3.zip');
whichVersion.forValue('v1.4').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v1.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.4.3-x86-linux.tar.gz');
whichVersion.forValue('v1.4').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v1.4').forValue('binary').forValue('Win32').addOptions('paraview-1.4.3-win32.exe');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.3.tar.Z');
whichVersion.forValue('v1.4').forValue('binary').forValue('hpux').addOptions('paraview-1.4.3-hpux-static.tar.gz');
whichVersion.forValue('v1.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.4.3-x86-linux.tar.Z');
whichVersion.forValue('v1.4').forValue('binary').forValue('irix64').addOptions('paraview-1.4.2-irix64.tar.gz');
whichVersion.forValue('v1.4').forValue('binary').forValue('hpux').addOptions('paraview-1.4.2-hpux-static.tar.Z');
whichVersion.forValue('v1.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.4.2-x86-linux.tar.gz');
whichVersion.forValue('v1.4').forValue('binary').forValue('irix64').addOptions('paraview-1.4.2-irix64.tar.Z');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.2.zip');
whichVersion.forValue('v1.4').forValue('binary').forValue('hpux').addOptions('paraview-1.4.2-hpux-static.tar.gz');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.2.tar.gz');
whichVersion.forValue('v1.4').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.4.2-x86-linux.tar.Z');
whichVersion.forValue('v1.4').forValue('binary').forValue('Win32').addOptions('paraview-1.4.2-win32.exe');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.2.tar.Z');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.1.tar.gz');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.1.tar.Z');
whichVersion.forValue('v1.4').forValue('source').forValue('all').addOptions('paraview-1.4.1.zip');
whichVersion.forValue('v1.2').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v1.2').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v1.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.2.1-x86-linux.tar.Z');
whichVersion.forValue('v1.2').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v1.2').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.2').forValue('data').forValue('all').addOptions('paraview-docs-1.2.1.tar.gz');
whichVersion.forValue('v1.2').forValue('binary').addOptionsTextValue('HP Unix', 'hpux');
whichVersion.forValue('v1.2').forValue('binary').forValue('hpux').addOptions('paraview-1.2.1-hpux-static.tar.Z');
whichVersion.forValue('v1.2').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v1.2').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.2').forValue('source').forValue('all').addOptions('paraview-1.2.1.tar.Z');
whichVersion.forValue('v1.2').forValue('data').forValue('all').addOptions('paraview-docs-1.2.1.zip');
whichVersion.forValue('v1.2').forValue('binary').addOptionsTextValue('Irix 64-bit', 'irix64');
whichVersion.forValue('v1.2').forValue('binary').forValue('irix64').addOptions('paraview-1.2.1-irix64.tar.gz');
whichVersion.forValue('v1.2').forValue('data').forValue('all').addOptions('paraview-docs-1.2.1.tar.Z');
whichVersion.forValue('v1.2').forValue('source').forValue('all').addOptions('paraview-1.2.1.zip');
whichVersion.forValue('v1.2').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v1.2').forValue('binary').forValue('Win32').addOptions('paraview-1.2.1-win32.exe');
whichVersion.forValue('v1.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.2.1-x86-linux.tar.gz');
whichVersion.forValue('v1.2').forValue('binary').forValue('irix64').addOptions('paraview-1.2.1-irix64.tar.Z');
whichVersion.forValue('v1.2').forValue('source').forValue('all').addOptions('paraview-1.2.1.tar.gz');
whichVersion.forValue('v1.2').forValue('binary').forValue('hpux').addOptions('paraview-1.2.1-hpux-static.tar.gz');
whichVersion.forValue('v1.2').forValue('source').forValue('all').addOptions('paraview-1.2.0.tar.Z');
whichVersion.forValue('v1.2').forValue('binary').forValue('hpux').addOptions('paraview-1.2.0-hpux-static.tar.Z');
whichVersion.forValue('v1.2').forValue('source').forValue('all').addOptions('paraview-1.2.0.tar.gz');
whichVersion.forValue('v1.2').forValue('binary').forValue('irix64').addOptions('paraview-1.2.0-irix64.tar.gz');
whichVersion.forValue('v1.2').forValue('binary').forValue('hpux').addOptions('paraview-1.2.0-hpux-static.tar.gz');
whichVersion.forValue('v1.2').forValue('binary').forValue('Win32').addOptions('paraview-1.2.0-win32.exe');
whichVersion.forValue('v1.2').forValue('data').forValue('all').addOptions('paraview-docs-1.2.0.tar.gz');
whichVersion.forValue('v1.2').forValue('data').forValue('all').addOptions('paraview-docs-1.2.0.zip');
whichVersion.forValue('v1.2').forValue('source').forValue('all').addOptions('paraview-1.2.0.zip');
whichVersion.forValue('v1.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.2.0-x86-linux.tar.Z');
whichVersion.forValue('v1.2').forValue('data').forValue('all').addOptions('paraview-docs-1.2.0.tar.Z');
whichVersion.forValue('v1.2').forValue('binary').forValue('irix64').addOptions('paraview-1.2.0-irix64.tar.Z');
whichVersion.forValue('v1.2').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.2.0-x86-linux.tar.gz');
whichVersion.forValue('v1.0').addOptionsTextValue('ParaView Binary Installers', 'binary');
whichVersion.forValue('v1.0').forValue('binary').addOptionsTextValue('HP Unix', 'hpux');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.4-hpux-static.tar.gz');
whichVersion.forValue('v1.0').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v1.0').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.4.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').addOptionsTextValue('Irix 64-bit', 'irix64');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.4-irix64.tar.Z');
whichVersion.forValue('v1.0').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v1.0').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.4.zip');
whichVersion.forValue('v1.0').forValue('binary').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v1.0').forValue('binary').forValue('Win32').addOptions('paraview-1.0.4-win32.exe');
whichVersion.forValue('v1.0').forValue('binary').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.4-x86-linux.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.4-hpux-static.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.4-x86-linux.tar.gz');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.4.tar.Z');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.4.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.4-irix64.tar.gz');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.4.zip');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.4.tar.Z');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.3.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.3-x86-linux.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.3-hpux-static.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.3-irix64.tar.gz');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.3.zip');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.3.tar.gz');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.3.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.3-irix64.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('Win32').addOptions('paraview-1.0.3-win32.exe');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.3.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.3-hpux-static.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.3-x86-linux.tar.Z');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.3.zip');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.2-irix64.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.2-hpux-static.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.2-irix64.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.2-x86-linux.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.2-hpux-static.tar.gz');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.2.zip');
whichVersion.forValue('v1.0').forValue('binary').forValue('Win32').addOptions('paraview-1.0.2-win32.exe');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.2.tar.gz');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.2.tar.Z');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.2.zip');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.2-x86-linux.tar.Z');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.2.tar.Z');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.2.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.1-irix64.tar.Z');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.1.zip');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.1-hpux-static.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('Win32').addOptions('paraview-1.0.1-win32.exe');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.1-x86-linux.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.1-hpux-static.tar.Z');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.1.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.1-irix64.tar.gz');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.1.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.1-x86-linux.tar.Z');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.1.tar.Z');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.1.tar.Z');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.1.zip');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.0-irix64.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.0-hpux-static.tar.gz');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.0.tar.gz');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.0.zip');
whichVersion.forValue('v1.0').forValue('binary').forValue('hpux').addOptions('paraview-1.0.0-hpux-static.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('irix64').addOptions('paraview-1.0.0-irix64.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.0-x86-linux.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('Linux-x86').addOptions('paraview-1.0.0-x86-linux.tar.gz');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.0.tar.Z');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('paraview-1.0.0.tar.Z');
whichVersion.forValue('v1.0').forValue('binary').forValue('Win32').addOptions('paraview-1.0.0-win32.exe');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.0.tar.gz');
whichVersion.forValue('v1.0').forValue('data').forValue('all').addOptions('paraview-docs-1.0.0.zip');whichVersion.forValue('v1.6').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v1.6').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.6').forValue('data').forValue('all').addOptions('ParaViewUsersGuide.PDF');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('ParaView-3.2.zip');
whichVersion.forValue('v3.8').addOptionsTextValue('Community Contributed Plugins', 'plugin');
whichVersion.forValue('v3.8').forValue('plugin').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.8').forValue('plugin').forValue('Linux-x86_64').addOptions('AcuSolveReaderPlugin-Linux64-x86.tar.gz');
whichVersion.forValue('v3.8').forValue('plugin').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.8').forValue('plugin').forValue('Linux-x86').addOptions('AcuSolveReaderPlugin-Linux32-x86.tar.gz');
whichVersion.forValue('v1.2').forValue('binary').forValue('Win32').addOptions('paraview-1.2-win32.rtf');
whichVersion.forValue('v3.2').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v3.2').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.2').forValue('data').forValue('all').addOptions('ParaViewData3.2.zip');
whichVersion.forValue('v1.4').forValue('binary').forValue('Win32').addOptions('paraview-1.4-win32.rtf');
whichVersion.forValue('v1.4').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v1.4').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.4').forValue('data').forValue('all').addOptions('ParaViewUsersGuide.PDF');
whichVersion.forValue('v3.8').forValue('plugin').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.8').forValue('plugin').forValue('Win32').addOptions('AcuSolveReaderPlugin-win32-x86.zip');
whichVersion.forValue('v3.6').addOptionsTextValue('Community Contributed Plugins', 'plugin');
whichVersion.forValue('v3.6').forValue('plugin').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.6').forValue('plugin').forValue('Win64').addOptions('AcuSolveReaderPlugin-win64-x86.zip');
whichVersion.forValue('v3.6').forValue('plugin').addOptionsTextValue('Linux 32-bit', 'Linux-x86');
whichVersion.forValue('v3.6').forValue('plugin').forValue('Linux-x86').addOptions('AcuSolveReaderPlugin-Linux32-x86.tar.gz');
whichVersion.forValue('v3.6').forValue('plugin').addOptionsTextValue('Linux 64-bit', 'Linux-x86_64');
whichVersion.forValue('v3.6').forValue('plugin').forValue('Linux-x86_64').addOptions('AcuSolveReaderPlugin-Linux64-x86.tar.gz');
whichVersion.forValue('v3.4').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v3.4').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.4').forValue('data').forValue('all').addOptions('ParaViewData3.4.zip');
whichVersion.forValue('v3.6').forValue('plugin').addOptionsTextValue('Windows 32-bit', 'Win32');
whichVersion.forValue('v3.6').forValue('plugin').forValue('Win32').addOptions('AcuSolveReaderPlugin-win32-x86.zip');
whichVersion.forValue('v2.6').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v2.6').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.6').forValue('data').forValue('all').addOptions('paraview-tutorial-data-2.6.tar.gz');
whichVersion.forValue('v1.0').forValue('binary').forValue('Win32').addOptions('paraview-1.0-win32.rtf');
whichVersion.forValue('v1.8').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v1.8').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v1.8').forValue('data').forValue('all').addOptions('paraview-tutorial-data-1.8.tar.gz');
whichVersion.forValue('v2.6').forValue('source').forValue('all').addOptions('paraview2.6.tgz');
whichVersion.forValue('v2.2').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v2.2').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.2').forValue('data').forValue('all').addOptions('paraview-tutorial-data-2.2.tar.gz');
whichVersion.forValue('v2.4').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v2.4').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.4').forValue('data').forValue('all').addOptions('paraview-tutorial-data-2.4.tar.gz');
whichVersion.forValue('v2.9').addOptionsTextValue('Source Code', 'source');
whichVersion.forValue('v2.9').forValue('source').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.9').forValue('source').forValue('all').addOptions('ParaView2-9-September.tgz');
whichVersion.forValue('v2.9').forValue('source').forValue('all').addOptions('ParaView2-9-September.zip');
whichVersion.forValue('v2.9').forValue('source').forValue('all').addOptions('ParaView2-9-August.zip');
whichVersion.forValue('v3.2').forValue('source').forValue('all').addOptions('paraview-3.2.tar.gz');
whichVersion.forValue('v3.8').forValue('plugin').addOptionsTextValue('Windows 64-bit', 'Win64');
whichVersion.forValue('v3.8').forValue('plugin').forValue('Win64').addOptions('AcuSolveReaderPlugin-win64-x86.zip');
whichVersion.forValue('v1.0').forValue('source').forValue('all').addOptions('ParaViewUpdate.ini');
whichVersion.forValue('v2.9').forValue('source').forValue('all').addOptions('ParaView2-9-August.tgz');
whichVersion.forValue('v2.9').forValue('source').forValue('all').addOptions('disk_out_ref.exo');
whichVersion.forValue('v3.6').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v3.6').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v3.6').forValue('data').forValue('all').addOptions('ParaViewData3.6.zip');
whichVersion.forValue('v1.4').forValue('data').forValue('all').addOptions('paraview-tutorial-data-1.4.tar.Z');
whichVersion.forValue('v2.0').addOptionsTextValue('Data, Documentation, and Tutorials', 'data');
whichVersion.forValue('v2.0').forValue('data').addOptionsTextValue('All', 'all');
whichVersion.forValue('v2.0').forValue('data').forValue('all').addOptions('paraview-tutorial-data-2.0.tar.gz'); whichVersion.forValue('v3.14').setDefaultOptions('binary');
whichVersion.forValue('v3.14').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.12').setDefaultOptions('binary');
whichVersion.forValue('v3.12').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.11').setDefaultOptions('binary');
whichVersion.forValue('v3.11').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.10').setDefaultOptions('binary');
whichVersion.forValue('v3.10').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.9').setDefaultOptions('binary');
whichVersion.forValue('v3.9').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.8').setDefaultOptions('binary');
whichVersion.forValue('v3.8').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.6').setDefaultOptions('binary');
whichVersion.forValue('v3.6').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.4').setDefaultOptions('binary');
whichVersion.forValue('v3.4').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.2').setDefaultOptions('binary');
whichVersion.forValue('v3.2').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.1').setDefaultOptions('binary');
whichVersion.forValue('v3.1').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v3.0').setDefaultOptions('binary');
whichVersion.forValue('v3.0').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v2.9').setDefaultOptions('binary');
whichVersion.forValue('v2.9').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v2.6').setDefaultOptions('binary');
whichVersion.forValue('v2.6').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v2.5').setDefaultOptions('binary');
whichVersion.forValue('v2.5').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v2.4').setDefaultOptions('binary');
whichVersion.forValue('v2.4').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v2.2').setDefaultOptions('binary');
whichVersion.forValue('v2.2').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v2.0').setDefaultOptions('binary');
whichVersion.forValue('v2.0').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v1.8').setDefaultOptions('binary');
whichVersion.forValue('v1.8').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v1.6').setDefaultOptions('binary');
whichVersion.forValue('v1.6').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v1.4').setDefaultOptions('binary');
whichVersion.forValue('v1.4').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v1.2').setDefaultOptions('binary');
whichVersion.forValue('v1.2').forValue('binary').setDefaultOptions('Win32');
whichVersion.forValue('v1.0').setDefaultOptions('binary');
whichVersion.forValue('v1.0').forValue('binary').setDefaultOptions('Win32');

// ----------------------------------------------------------------------------
// Page initialization
// ----------------------------------------------------------------------------

jQuery(document).ready(function() {
  // Fill version list 
  jQuery('select[name=version]').append(jQuery(versions));

  // Fill dependency select
  jQuery('select[name=type]').append(jQuery("<script type='text/javascript'>whichVersion.printOptions('type');</script>"));
  jQuery('select[name=os]').append(jQuery("<script type='text/javascript'>whichVersion.printOptions('os');</script>"));
  jQuery('select[name=downloadFile]').append(jQuery("<script type='text/javascript'>whichVersion.printOptions('downloadFile');</script>"));

  initDynamicOptionLists();
  updateToCurrentOSLater();

  jQuery('select').change(invalidateDownloadLink);
  jQuery('select[name=version]').change(updateToCurrentOSLater);
  jQuery('select[name=type]').change(updateToCurrentOSLater);

  jQuery('.direct-link').click(function(){
    var me = jQuery(this);
    updateDownloadLinks();
    jQuery('.direct-download-link', me.parent()).show();
  });

  jQuery.ajax({url: "/files/nightly/",
    success : function(data) {
      var nightlyFiles = parseFileList(data);
      // sort the list of files alphabetically.
      nightlyFiles.sort();
      var buffer = [];
      for(var idx in nightlyFiles) {
        buffer.push('<option>');
        buffer.push(nightlyFiles[idx]);
        buffer.push('</option>');
      }
      jQuery('.nightly select').append(jQuery(buffer.join('')));
    }});
});

function parseFileList(data) {
  var list = [];
  var regexg = /<a href="([^"]*\.((?:gz)|(?:tgz)|(?:zip)|(?:dmg)|(?:exe)|(?:pdf)))">/g;
  var match;
  while ((match = regexg.exec(data)) != null) {
    list.push(match[1]);
  }
  return list;
}

